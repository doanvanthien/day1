/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thiendv;

import java.util.Scanner;

/**
 *
 * @author thiendv
 */
public class Main {

    public static void main(String[] args) {
        //declare variable 
        Scanner scan = new Scanner(System.in);
        Manage manage = new Manage();

        //Create list student
        for (int i = 0; i < 2; i++) {
            Student student = new Student();
            student.input();
            manage.addStudent(student);
        }
        // function display list student
        manage.displayListStudent();

        // function search name student
        System.out.println("Nhap ten can tim: ");
        String neededFind = scan.nextLine();
        Student student = manage.searchNameStudent(neededFind);

        if (student != null) {
            student.display();
        } else {
            System.out.println("Not found name student.");
        }

        // function delete student follow name student
        System.out.println("Nhap ten can xoa: ");
        String neededDelete = scan.nextLine();
        manage.deleteNameStudent(neededDelete);
        manage.displayListStudent();
        
        // function edit student follow name student
        System.out.println("Nhap ten can chinh sua: ");
        String neededEdit = scan.nextLine();
        manage.editInfoStudentFollowName(neededEdit);
        manage.displayListStudent();
        
        
    }

}

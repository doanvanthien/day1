/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thiendv;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author thiendv
 */
public class Student {

//  Fields
    private String name;
    private String studentID;
    private int mark;
    private int age;
    private Scanner scan = new Scanner(System.in);

//  accessor
    public Student() {
    }

    public Student(String name, String studentID, int mark, int age) {
        this.name = name;
        this.studentID = studentID;
        this.mark = mark;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public int getMark() {
        return mark;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentID() {
        return studentID;
    }

//  Methods
    public void input() {

        System.out.println("Enter info student: Name,studentID,age,mark ");

        this.name = scan.nextLine();
        setName(name);

        this.studentID = scan.nextLine();
        setStudentID(studentID);

        this.age = scan.nextInt();
        setAge(age);

        this.mark = scan.nextInt();
        setMark(mark);

    }

    public void display() {
        
        System.out.println(this.name + " " + this.studentID + " " + this.age + " " 
                            + this.mark + " ");
        
    }

}

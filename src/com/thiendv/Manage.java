/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thiendv;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author thiendv
 */
public class Manage {
    //fields

    private ArrayList<Student> listStudent;

    // accessor
    public Manage() {

        listStudent = new ArrayList<Student>();

    }

    // methods
    //method add student in list student
    public void addStudent(Student student) {

        listStudent.add(student);

    }

    //method search name student
    public Student searchNameStudent(String nameStudent) {
        String tempName;

        for (int i = 0; i < listStudent.size(); i++) {
            //listStudent.get(i) return object student
            // method getName return value name student
            tempName = (listStudent.get(i)).getName();
            if (tempName.compareToIgnoreCase(nameStudent) == 0) {
                return listStudent.get(i);

            }
        }

        return null;
    }

    //method search id student
    public Student searchIDStudent(String studentID) {
        String tempName;

        for (int i = 0; i < listStudent.size(); i++) {
            //listStudent.get(i) return object student. 
            // method getStudentID return value id student
            tempName = (listStudent.get(i)).getStudentID();
            if (tempName.compareToIgnoreCase(studentID) == 0) {
                return listStudent.get(i);

            }
        }

        return null;
    }

    //method edit info student
    public void editInfoStudent(Student student) {

        Scanner scan = new Scanner(System.in);
        String tempString;
        int tempInt;

        System.out.println("Nhap thong tin can chinh sua: ");

        // edit name
        System.out.println("Chinh sua ten: ");
        tempString = scan.nextLine();
        if (tempString.compareTo("") != 0) {
            student.setName(tempString);
        }

        //edit id
        System.out.println("Chinh sua Id: ");
        tempString = scan.nextLine();
        if (tempString.compareTo("") != 0) {
            student.setStudentID(tempString);
        }

        //edit age
        System.out.println("Chinh sua tuoi: ");
        tempInt = scan.nextInt();
        if (tempInt > 0) {
            student.setAge(tempInt);
        }

        //edit mark
        System.out.println("Chinh sua diem: ");
        tempInt = scan.nextInt();
        if (tempInt > 0) {
            student.setMark(tempInt);
        }

    }

    //method edit info sutdent follow name student
    public void editInfoStudentFollowName(String nameStudent) {
        Student student = this.searchNameStudent(nameStudent);

        if (student != null) {
            this.editInfoStudent(student);
        } else {
            System.out.println("Don't exist name student. So, we can't edit info student");
        }

    }

    //method edit info student follow id student
    public void editInfoStudentFollowID(String studentID) {
        Student student = this.searchIDStudent(studentID);

        if (student != null) {
            this.editInfoStudent(student);
        } else {
            System.out.println("Don't exist id student. So, we can't edit info student ");
        }
        
    }

    //method delete student 
    public void deleteStudent(Student student) {
        if (listStudent.contains(student)) {
            listStudent.remove(student);
        }
        
    }

    //method delete student follow name student
    public void deleteNameStudent(String nameStudent) {

        Student student = this.searchNameStudent(nameStudent);

        if (student != null) {
            deleteStudent(student);
        } else {
            System.out.println("Name student don't exit. So, we can't delete object. ");
        }

    }

    //method display list student
    public void displayListStudent() {

        for (int i = 0; i < listStudent.size(); i++) {
            System.out.println("Info student " + i + 1 + ": name studentID age mark ");
            (listStudent.get(i)).display();

        }

    }

}
